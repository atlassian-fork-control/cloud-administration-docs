---
title: "Tutorial title"
platform: platform
product: cloud-administration
category: devguide
subcategory: templates
date: "2018-11-7"
---

[This template is sourced from the Writing Toolkit.](https://developer.atlassian.com/platform/writing-toolkit/tutorial-page/)

Instructions for using this template:

 - Remove the HTML comments.
 - Remove optional sections that you are not using.
 - Ensure you update the YAML metadata (above).
 - Update headings, page title, etc.

NOTE: Use http://example.atlassian.net as a sample URL. We own this domain.

# Tutorial title (e.g., Getting started)

Provide the reader with a brief overview and introduce the concepts.
Link to any relevant product functionality the user may need to reference (e.g., JQL or API documentation).

Include tutorial-specific visuals at the beginning to illustrate complex concepts and expected results.
For example, add a screenshot that shows how the sample app used in your tutorial works with
an Atlassian product.

## Before you begin

List prerequisites to completing the tutorial in this section, including:

 - Software the user must install before starting (e.g., Git)
 - Tutorials the user should have completed (e.g., Getting started or developer setup)

This section is required.

## Step 1:

Chunk major steps into sections labeled "Step: # (task)".
Each major step must include at least one procedure and may contain multiple, closely-related tasks.
Provide context (description) for each procedure.

If you do not have multiple steps, then you may omit the "Step: #" labeling.

## Step 2:

Add a description and procedure, repeating this pattern until all steps are covered.

Conclude with a brief recap of the results (you may choose to add additional screenshots here.)

## Next steps

Always end tutorials with this section. Provide at least one useful next step, such as a
tutorial or further reading. Limit it to a few choices so you don’t overwhelm the reader with
options.

An example of a next step paragraph appears below:

Now that you have set up your cloud development site, you're ready to get started building apps.
Check out the [Sample app](/cloud/confluence/sample-app), or take a look at the
[REST API](/cloud/confluence/rest/) docs.